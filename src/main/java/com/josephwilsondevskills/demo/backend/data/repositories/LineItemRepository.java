package com.josephwilsondevskills.demo.backend.data.repositories;

import com.josephwilsondevskills.demo.backend.data.models.LineItem;
import com.josephwilsondevskills.demo.backend.data.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.sound.sampled.Line;
import java.math.BigDecimal;
import java.util.List;

public interface LineItemRepository extends JpaRepository<LineItem, Long> {
    List<LineItem> findByOrder(Order order);
}
