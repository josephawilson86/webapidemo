package com.josephwilsondevskills.demo.backend.data.repositories;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
//
//    List<Customer> findByEmail(String email);
//
//    List<Customer> findByname(String name);

}
