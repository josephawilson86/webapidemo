package com.josephwilsondevskills.demo.backend.data.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="orders") // order is protected? maybe because of 'order by' ? :(
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Customer customer;
}
