package com.josephwilsondevskills.demo.backend.data.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Order order;

    @NotNull
    private String itemId;

    @NotNull
    private String description;

    @NotNull
    private BigDecimal unitPrice;

    @NotNull
    private int units;




}
