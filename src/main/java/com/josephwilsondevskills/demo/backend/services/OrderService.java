package com.josephwilsondevskills.demo.backend.services;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import com.josephwilsondevskills.demo.backend.data.models.Order;
import com.josephwilsondevskills.demo.backend.data.repositories.CustomerRepository;
import com.josephwilsondevskills.demo.backend.data.repositories.OrderRepository;
import com.josephwilsondevskills.demo.backend.webapi.v1.CreateOrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CustomerRepository customerRepository;

    public Order getById(Long orderId) {
        return orderRepository.findById(orderId).get();
    }

    public List<Order> getOrders(Long customerId) {
        Customer customer = customerRepository.findById(customerId).get();
        return orderRepository.findByCustomer(customer);
//        return new ArrayList<>();
    }

    public Order createNewOrder(CreateOrderRequest createOrderRequest) {
        Customer customer = customerRepository.findById(createOrderRequest.getCustomer()).get();
        Order order = new Order();
        order.setCustomer(customer);
        return orderRepository.save(order);
    }

    public List<Order> getAll() {
        return orderRepository.findAll();
    }
}
