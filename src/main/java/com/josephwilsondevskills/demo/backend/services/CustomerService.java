package com.josephwilsondevskills.demo.backend.services;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import com.josephwilsondevskills.demo.backend.data.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    public Customer getById(Long customerId) {
        return customerRepository.findById(customerId).get();
    }

    public Customer createNewCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer)  {
        return customerRepository.save(customer);
    }

    public List<Customer> getAll() {
        return customerRepository.findAll();
    }
}
