package com.josephwilsondevskills.demo.backend.services;

import com.josephwilsondevskills.demo.backend.data.models.LineItem;
import com.josephwilsondevskills.demo.backend.data.models.Order;
import com.josephwilsondevskills.demo.backend.data.repositories.LineItemRepository;
import com.josephwilsondevskills.demo.backend.data.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LineItemService {

    @Autowired
    LineItemRepository lineItemRepository;
    OrderRepository orderRepository;

    public List<LineItem> getByOrder(Long orderId) {
        Order order = orderRepository.findById(orderId).get();
        return lineItemRepository.findByOrder(order);
    }

    public LineItem getById(Long id) {
        return lineItemRepository.findById(id).get();
    }
}
