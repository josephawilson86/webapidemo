package com.josephwilsondevskills.demo.backend.webapi.v1;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import com.josephwilsondevskills.demo.backend.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomersController {

    @Autowired
    CustomerService customerService;

    @GetMapping
    public List<Customer> getCustomers(){
        return customerService.getAll();
    }
}
