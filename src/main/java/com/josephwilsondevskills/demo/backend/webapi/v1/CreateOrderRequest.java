package com.josephwilsondevskills.demo.backend.webapi.v1;

import lombok.Data;

@Data
public class CreateOrderRequest {
    private Long customer;
}
