package com.josephwilsondevskills.demo.backend.webapi.v1;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import com.josephwilsondevskills.demo.backend.data.models.Order;
import com.josephwilsondevskills.demo.backend.services.CustomerService;
import com.josephwilsondevskills.demo.backend.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping( path = "/api/v1/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @Autowired
    OrderService orderService;

    @GetMapping("/test")
    public Customer getTest() {
        return new Customer();
    }

    // TODO: return a Response with proper codes instead of Optional<T>
    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {
        return customerService.getById(id);
    }

    @GetMapping("{id}/orders")
    public List<Order> getOrders(@PathVariable Long id) {
        return orderService.getOrders(id);
    }

    @PostMapping
    public Customer create(@RequestBody Customer customer) {
        return customerService.createNewCustomer(customer);
    }

    // TODO: return a Response with proper codes if they pass an id that doesn't exist
    @PutMapping
    public Customer update(@RequestBody Customer customer) {
        return customerService.updateCustomer(customer);
    }

    // No delete endpoint. Companies don't generally delete data. In the real world there would be an 'active'
    // property or something like that.


}
