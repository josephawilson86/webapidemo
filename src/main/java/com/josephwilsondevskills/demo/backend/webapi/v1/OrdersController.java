package com.josephwilsondevskills.demo.backend.webapi.v1;

import com.josephwilsondevskills.demo.backend.data.models.Order;
import com.josephwilsondevskills.demo.backend.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
public class OrdersController {

    @Autowired
    OrderService orderService;

    @GetMapping
    public List<Order> getOrders() {
        return orderService.getAll();
    }
}
