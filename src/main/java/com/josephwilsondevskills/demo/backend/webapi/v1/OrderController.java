package com.josephwilsondevskills.demo.backend.webapi.v1;

import com.josephwilsondevskills.demo.backend.data.models.Customer;
import com.josephwilsondevskills.demo.backend.data.models.LineItem;
import com.josephwilsondevskills.demo.backend.data.models.Order;
import com.josephwilsondevskills.demo.backend.services.LineItemService;
import com.josephwilsondevskills.demo.backend.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    @Autowired
    OrderService orderService;
    LineItemService lineItemService;

    @GetMapping("/test")
    public Order getTestOrder(){ return new Order();}

    @GetMapping("/{id}")
    public Order getById(@PathVariable Long id) {
        return orderService.getById(id);
    }

    @GetMapping("/{id}/lineitems")
    public List<LineItem> getLineItems(@PathVariable Long id) {
        return lineItemService.getByOrder(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Order create(@RequestBody CreateOrderRequest createOrderRequest) {
        return orderService.createNewOrder(createOrderRequest);
    }

    // no put mapping, nothing to update
}
