package com.josephwilsondevskills.demo.backend.webapi.v1;

import com.josephwilsondevskills.demo.backend.data.models.LineItem;
import com.josephwilsondevskills.demo.backend.services.LineItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/lineitem")
public class LineItemController {

    @Autowired
    LineItemService lineItemService;

    @GetMapping("/test")
    public LineItem getTest() {
        return new LineItem();
    }

    @GetMapping("/{id}")
    public LineItem getLineItem(@PathVariable Long id) {
        return lineItemService.getById(id);
    }


}
